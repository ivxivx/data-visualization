//dataobject, property, and uiobject should not contain space and -
//SvgBinding and CanvasBinding
function Binding(dataobject) {
	var binding = {};//createObject(Binding.prototype);

	binding.dataobject = dataobject;

	var bindings = {};

	function bind(type, arg1, arg2) {
		if (!arguments.length)
			return binding;

		//console.log(type, arg1, arg2, bindings[type]);

		if (typeof arg1 == "function") {
			bindings[type] = {};

			bindings[type].name = arg2 ? arg2 : "function";
			bindings[type].func = arg1;

			return binding;
		}
		else if (typeof arg1 == "string") {
			if (bindings[type]) {
				bindings[type].name = arg1;

				if (arg2) {
					if (bindings[type].mapping) {
						for (var m in bindings[type].mapping) {
							//console.log("mapping item: " + m + ", " + other.mapping[m] + ", " + binding.mapping[m])
							if (!arg2[m]) {
								arg2[m] = bindings[type].mapping[m];
							}
						}
					}
					else {
						bindings[type].mapping = arg2;
					}
					bindings[type].func = function(d) {
						return arg2[d[arg1]];
					}
				}
				else {
					bindings[type].func = function(d) {
						return arg1;
					}
				}
			}
			else {
				bindings[type] = {};

				bindings[type].name = arg1;
				if (arg2) {
					bindings[type].func = function(d) {
						return arg2[d[arg1]];
					}
					bindings[type].mapping = arg2;
				}
				else {
					bindings[type].func = function(d) {
						return arg1;
					}
				}
			}

			return binding;
		}
		else {
			return bindings[type].func(arg1);
		}
	}

	binding.graphics = function(arg1, arg2) {
		return bind("graphics", arg1, arg2);
	}
	
	binding.fill = function(arg1, arg2) {
		return bind("fill", arg1, arg2);
	}
	/*
	binding.size = function(arg1, arg2) {
		return bind("size", arg1, arg2);
	}*/
	
	binding.opacity = function(arg1, arg2) {
		return bind("fill-opacity", arg1, arg2);
	}

	return binding;
};
/*
mutating the [[Prototype]] of an object will cause your code to run very slowly; instead create the object with the correct initial [[Prototype]] value using Object.create d3.js:553:4
*/
/*
var createObject = function (o) {
  function F() {}
  F.prototype = o;
  return new F();
};*/