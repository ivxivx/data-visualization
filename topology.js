function Topology(containerId, length) {
	var topology = {};

	var square_root_of_3 = 1.7320508
//TODO use lenght as diameter or size length of square
	if (arguments.length == 2) {
		var _length = length;
	}
	else  {
		var _length = 25;
	}

	var uiobjects = {};
	uiobjects.circle = {"type": "circle", "attributes": {"r": _length/2, "cx": _length/2, "cy": _length/2}};
	uiobjects.ellipse = {"type": "ellipse", "attributes": {"rx": _length/2, "ry": _length/3, "cx": _length/2, "cy": _length/2}};
	uiobjects.square = {"type": "rect", "attributes": {"x": 0, "y": 0, "width": _length, "height": _length}};
	uiobjects.triangle = {"type": "polygon", "attributes": {"points": _length/2 + ",0 0," + _length*square_root_of_3/2 + " " + _length + "," + _length*square_root_of_3/2}};
	uiobjects.hexagon = {"type": "polygon", "attributes": {"points": "0," + _length/2 + " " + _length/4 + ",0 " + _length*3/4 + ",0 " + _length + "," + _length/2 + " " + _length*3/4 + "," + _length + " " + _length/4 + "," + _length}};
	uiobjects.text = {"type": "text", "attributes": {"dx": _length/2, "dy": _length/2+30, "text-anchor": "middle"}};		

	var nodeHalfSize = 25;
	var nodeImageHalfSize = 15;

	var statusColorMap = {
		critical: "red",
		major: "orange",
		minor: "yellow",
		clear: "green",
		unknown: "blue",
		unmanaged: "grey",
		up: "green",
		down: "red"
		//1, "green"
	};

	var bindings = [];

	topology.bind = function(dataobject) {
		var binding;
		bindings.forEach(function(e, i) {
			if (e.dataobject == dataobject) {
				binding = bindings[i];
			}
		});

		if (!binding) {
			binding = new Binding(dataobject);
			bindings.push(binding);
		}

		return binding;
	}

	var addDefaultBindings = function() {

		//console.log("typeof: " + (typeof b1));
		//console.log("instanceof: " + (b1 instanceof Binding));

		topology.bind("nodes").graphics("circle").fill("status", statusColorMap).opacity("1");
	}

	addDefaultBindings();	

	var container = d3.select(containerId).node();
	var w = parseFloat(container.clientWidth);
	var h = parseFloat(container.clientHeight);

	var svg = d3.select(container).append("svg").attr("id", "topology-svg").attr("width", w).attr("height", h).style("background", "#999999");
	var svgDef = svg.append("defs");
	var rootG = svg.append("g").attr("id", "topology-g");

	var onTick = function() {
		rootG.selectAll("g.topology-node").attr("transform", function(e) {
			return "translate(" + e.x + "," + e.y + ")";
		});

		rootG.selectAll("line.topology-link")
		.attr("x1", function(e) {
			return e.source.x + nodeHalfSize;})
		.attr("y1", function(e) {return e.source.y + nodeHalfSize;})
		.attr("x2", function(e) {return e.target.x + nodeHalfSize;})
		.attr("y2", function(e) {return e.target.y + nodeHalfSize;})

		svgDef.selectAll("rect")
		.attr("x", function(e) {
			return e.x;})
		.attr("y", function(e) {
			return e.y;});
	};

	var force = d3.layout.force().size([w, h]).gravity(.5).charge(-1000).distance(function(link) {
		if (link.parentchildlink) {
			if (link.target.type == "Interface") {
				return -50;
			}
		}

		return 100;
	})
	.on("tick", onTick);

	var nodes = force.nodes(), links = force.links();
	var nodeHash = {};

	var update = function() {
		/*
		var masks = d3.select("svg").select("defs").selectAll("mask").data(nodes, function(e) {
			return e.id;
		});

		masks.enter()
		.append("mask")
				.attr("id", function(e, i) {
					return "topology-mask" + i;
				})
				.append("rect")
				.attr("fill", "#000000")
				.attr(uiobjects["square"].attributes)
				.transition().duration(2000)
				.attr("fill", "#FFFFFF");
		*/

		//draw links first, so they will be drawn behind nodes
		var d3Links = rootG.selectAll("line.topology-link")
		.data(links, function(e) {
			return e.sourceid + "-" + e.targetid;
		});

		d3Links.exit().remove();

		d3Links.enter()
		.append("line")
		.attr("class", "topology-link")
		.style("stroke", "black");

		var d3Nodes = rootG.selectAll("g.topology-node")
		.data(nodes, function(e) {
			return e.id;
		});

		d3Nodes.exit().remove();

		d3Nodes.enter().append("g").attr("class", "topology-node")
		.call(force.drag);

		bindings.forEach(function(b) {
			d3Nodes.append(function(e, i) {
				return document.createElementNS(d3.ns.prefix.svg, uiobjects[b.graphics(e)].type);
			})
			.attr("class", "topology-" + b.dataobject + "-graphics")			
			.each(function(e, i) {
				d3.select(this)
				.style("fill-opacity", 0)
				.transition().duration(2000)
				.attr(uiobjects[b.graphics(e)].attributes)
				.style("fill", b.fill(e))
				.style("fill-opacity", b.opacity(e));
			})
			/*
			.each(function(e, i) {
				//console.log(b, "|", b.attrFunction);
				if (b.attribute) {
					d3.select(this).attr(b.attribute, b.attrFunction(e));
				}
			})*/
			.text(function(e) {
				return e[b.property];
			});

			d3Nodes.append("text")
			.attr("class", "topology-node-text")
			.text(function(e) {
				return e.name;
			})
			.attr({"dx": 25, "dy": 60})
			.style({"text-anchor": "middle"})
			.attr("fill", "red");
		});


/*
		d3Nodes.append("image")
		.attr("class", "topology-node-image")				
		.attr("xlink:href", function (e) {
			return "./images/" + e.type.toLowerCase() + "_icon.png";
		})
		.style("mask", function (e, i) {
			return "url(#" + "topology-mask" + i + ")";
		})
		.attr("fill-opacity", 1)
		.attr("x", nodeHalfSize - nodeImageHalfSize)
		.attr("y", nodeHalfSize - nodeImageHalfSize)
		.attr("width", nodeImageHalfSize * 2)
		.attr("height", nodeImageHalfSize * 2);
*/
		force.start();
	}

	topology.addNode = function(node, toUpdate) {
		if (!arguments.length)
			return;

		nodes.push(node);
		nodeHash[node.id] = node;

		if (toUpdate) {
			update();
		}

		return topology;
	}

	topology.addNodes = function(nodes, toUpdate) {
		if (!arguments.length)
			return;

		if (nodes.length) {
			nodes.forEach(function(node) {
				topology.addNode(node);
			});
		}
		else {
			topology.addNode(nodes);
		}

		if (nodes.length) {
			nodes.forEach(function(node) {
				if (node.parentid) {
					var parent = nodeHash[node.id];
					if (parent) {
						var link = {"sourceid": node.parentid, "targetid": node.id, "status": "na", "parentchildlink": true};

						topology.addLink(link, false);
					}
				}
			});
		}

		if (toUpdate) {
			update();
		}

		return topology;
	}

	topology.addLink = function(link, toUpdate) {
		if (!arguments.length)
			return;

		links.push(link);
		//link.weight = link.priority;
		link.weight = 5;
		link.source = nodeHash[link.sourceid];
		link.target = nodeHash[link.targetid];

		if (toUpdate) {
			update();
		}

		return topology;
	}

	topology.addLinks = function(links, toUpdate) {
		if (!arguments.length)
			return;

		if (links.length) {
			links.forEach(function(link) {
				topology.addLink(link);
			});
		}
		else {
			topology.addLink(link);
		}

		if (toUpdate) {
			update();
		}

		return topology;
	}

	topology.addNetwork = function(network) {
		//if (Object.prototype.toString.call(network.nodes) === '[object Array]')	{
		topology.addNodes(network.nodes);
		topology.addLinks(network.links);

		update();	
	}

	return topology;
}